//
//  Stars.h
//  Lection2
//
//  Created by Admin on 28.10.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface Stars : SPISpaceObject

@property (nonatomic, assign, getter = whatWeight) NSNumber* weight;

typedef NS_ENUM(NSInteger, StarType) {
    StarTypeBig,
    StarTypeSmall,
    StarTypeDefault
};

@property (nonatomic, assign) StarType type1;
@property (nonatomic, strong, readonly) NSString *name1;

- (instancetype)initWithType:(StarType)type1 name1:(NSString *)name1;


//- (NSString *)title;

@end
