//
//  StarSystem.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 05/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SPIStarSystem : NSObject

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSNumber *age;


@property (nonatomic, strong) NSMutableArray *spaceObjects;

- (instancetype)initWithName:(NSString *)name age:(NSNumber *)age;

@end
