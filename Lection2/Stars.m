//
//  Stars.m
//  Lection2
//
//  Created by Admin on 28.10.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Stars.h"

@implementation Stars


- (instancetype)initWithName:(NSString *)name {
    self = [super initWithType:SPISpaceObjectTypeStars name:name];
    if (self) {
        
    }
    return self;
}

- (NSNumber*) whatWeight{
    return _weight;
}

+ (NSString *)typeStringForType:(StarType)type1{
    switch (type1) {
        case StarTypeBig:
            return @"Big Star";
            
        case StarTypeSmall:
            return @"Small Star";
            
        default:
            return @"Unknown";
    }
}

- (instancetype)initWithType:(StarType)type1 name1:(NSString *)name1 {
    //self = [super init];
    
    self = [super initWithType:SPISpaceObjectTypeStars name:name1];
    
    if (self) {
        _type1 = type1;
        _name1 = name1;
    }
    return self;
}


/*- (NSString *)title {
    return [NSString stringWithFormat:@"%@ %@", [Stars typeStringForType: self.type1], self.name1];
}*/


- (NSString *)description {
    return [NSString stringWithFormat:@"\n Star: %@\n Weight: %@\n Type: %@ \nDestroing: %@",
            self.name1, self.weight, [Stars typeStringForType:self.type1], self.dest ? @"YES" : @"NO"];
}


@end
