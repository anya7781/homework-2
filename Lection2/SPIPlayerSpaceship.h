//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGameObject.h"


typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject,
    SPIPlayerSpaceshipCommandDestroyCurrentObject,
    SPIPlayerSpaceshipCommandFlyToNextObject,
    SPIPlayerSpaceshipCommandFlyToAnotherStarSystem, ///////
   
    SPIPlayerSpaceshipCommandExit = 42,
};

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
};

@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign) SPIPlayerSpaceshipCommand nextCommand;
@property (nonatomic, strong) NSArray *starSystems;   //array of the objects
@property (nonatomic, strong) SPIStarSystem *starSystem;
@property (nonatomic, strong) NSArray *systems_name;   // array of the objects names


- (void) loadStarSystem:(NSArray*) starSystem systems_name:(NSArray *) systems_name;

@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;

- (instancetype)initWithName:(NSString *)name;

- (NSString *)availableCommands;

- (SPIPlayerSpaceshipResponse)waitForCommand;

@end
