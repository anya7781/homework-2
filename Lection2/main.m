//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "Stars.h"

int main(int argc, const char * argv[]) {
    
     SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Alfa" age:@(230000000)];
     SPIStarSystem *betaStarSystem = [[SPIStarSystem alloc] initWithName:@"Beta" age:@(200000)];
     SPIStarSystem *gammaStarSystem = [[SPIStarSystem alloc] initWithName:@"Gamma" age:@(2400000000)];
     SPIStarSystem *akonoStarSystem = [[SPIStarSystem alloc] initWithName:@"Akono" age:@(830000000)];
    

    NSArray *name_system = @[alphaStarSystem.name, betaStarSystem.name, gammaStarSystem.name, akonoStarSystem.name]; //// names
    NSArray *systems = @[alphaStarSystem, betaStarSystem, gammaStarSystem, akonoStarSystem]; //// objects

    
    //creating objects
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;
    vulkanPlanet.dest = YES;
  
    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    hotaAsteroidField.dest = NO;
    
    SPIAsteroidField *firstAsteroidField = [[SPIAsteroidField alloc] initWithName:@"FirstAsteroid"];
    firstAsteroidField.density = 40000000;
    firstAsteroidField.dest = YES;
    
    SPIAsteroidField *secondAsteroidField = [[SPIAsteroidField alloc] initWithName:@"SecondAsteroid"];
    secondAsteroidField.density = 300000;
    secondAsteroidField.dest = YES;
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    gallifreiPlanet.dest = NO;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    nabooPlanet.dest =  YES;
    
    SPIPlanet *UpiterPlanet = [[SPIPlanet alloc] initWithName:@"Upiter"];
    UpiterPlanet.atmosphere = YES;
    UpiterPlanet.peoplesCount = 625000000;
    UpiterPlanet.dest = NO;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
    plutoPlanet.dest = YES;
    
    
    Stars *oneStar = [[Stars alloc] initWithType: StarTypeBig name1:@"Sun" ]; //creating a star
    oneStar.weight = @(1000000);
    oneStar.dest = NO;
    
    Stars *secondStar = [[Stars alloc] initWithType:StarTypeSmall name1: @"NotSun"];
    secondStar.weight = @(24500000);
    secondStar.dest = YES;
   
    
    //systems content
     alphaStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[vulkanPlanet, hotaAsteroidField, gallifreiPlanet, nabooPlanet]];
     betaStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[UpiterPlanet, secondStar, secondAsteroidField]];
     gammaStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[firstAsteroidField, oneStar, plutoPlanet]];
     akonoStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[]];
    
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon"];
    [spaceship loadStarSystem: systems systems_name: name_system];
   
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:betaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:gammaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:akonoStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];
    
    
     BOOL play = YES;

    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}


