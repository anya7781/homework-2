//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"
#import "Stars.h"
#import "SPISpaceObject.h"
#import "SPIAsteroidField.h"
#import "SPIPlanet.h"

@implementation SPIPlayerSpaceship

bool c = YES;
bool destroy = NO;

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)loadStarSystem:(NSArray *)starSystems systems_name:(NSArray *) systems_name {
    _starSystems = starSystems;
    _starSystem = starSystems.firstObject;
    _currentSpaceObject = _starSystem.spaceObjects.firstObject;
    _systems_name = systems_name;
    }

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];
    
    if (_starSystem == nil){
        NSLog (@"\nThis Star System is Empty\n");
        c = NO;
    }
        
        
    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command); //string into int
    self.nextCommand = commandNumber;
  
    
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
    
}

- (void)nextTurn {
    
    if (c == NO){  //if I choose another star system
        _starSystem = [_starSystems objectAtIndex:_nextCommand - 1];
        
        c = YES;
        _nextCommand = 0;
        if ([_starSystem.spaceObjects count] == 0)  _starSystem = nil;  //is any objects in system?
        _currentSpaceObject = _starSystem.spaceObjects.firstObject;
    }
   
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
         NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
           if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
           else if (currentSpaceObjectIndex == 0)   ///////
                c = NO;      //////
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            else if (currentSpaceObjectIndex == (self.starSystem.spaceObjects.count - 1))   ///////
                c = NO;
            
            break;
        }
        case SPIPlayerSpaceshipCommandDestroyCurrentObject: {
            destroy = YES;
            break;
        }
            
        default:
            break;
    }
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
  
    
     if (destroy == YES){
        NSString *type3 = [NSString stringWithFormat:@"%@", [self.currentSpaceObject type_name]];
        NSString *name3 = [NSString stringWithFormat:@"%@", [self.currentSpaceObject name_name]];
        NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
         NSString *d = [self.currentSpaceObject destroing];
         if ([d  isEqual: @"YES"])
         {

        if ([type3  isEqual: @"Planet"]){
            
            SPIAsteroidField *new = [[SPIAsteroidField alloc] initWithName: [NSString stringWithFormat:@"%@", name3]];
            [self.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:(SPIAsteroidField*) new];
            self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
            self.currentSpaceObject.dest = YES;
            
            NSLog(@"\nObject is destroyed\n");
            destroy = NO;
        }
         
        else if ([type3  isEqual: @"Star"]){
            SPIPlanet *new = [[SPIPlanet alloc] initWithName:[NSString stringWithFormat:@"%@", name3]];
            [self.starSystem.spaceObjects replaceObjectAtIndex:currentSpaceObjectIndex withObject:(SPIPlanet*) new];
            self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
            self.currentSpaceObject.dest = YES;
            NSLog(@"\nObject is destroyed\n");
              destroy = NO;
        }
         
        else if ([type3  isEqual: @"Asteroid field"]){
            [self.starSystem.spaceObjects removeObjectAtIndex:currentSpaceObjectIndex];
            
            if ([self.starSystem.spaceObjects count] == 0)
                c = NO;
            
            else if (currentSpaceObjectIndex == [self.starSystem.spaceObjects count]){
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
                 destroy = NO;
            }

            else if (currentSpaceObjectIndex >= 0){
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex];
                 destroy = NO;
            }
            NSLog(@"\nObject is deleted\n");
        }
    }
        
         else {
             NSLog (@"\nYou cannot destroy this object\n");
             destroy = NO;
         }
    }
    
    if (c == NO){    ////////// list of star systems
        
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        if (destroy == YES){
             [mutableDescriptions addObject:[NSString stringWithFormat:@"\nThis Star System is Empty\n"]];
            destroy = NO;
        }
        
        
        NSInteger index1 = [self.systems_name indexOfObject: self.systems_name.firstObject];
        
        for (NSInteger a = index1; a < [_systems_name count]; a++) {
                NSString *name1 = [self.systems_name objectAtIndex: a];
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. Fly to %@ star system", (long)a + 1,  name1]];
        }
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }//////////
    
   
     else if (self.starSystem) {
      
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandFlyToNextObject; command++) {
            NSString *description = [self descriptionForCommand:command];
            if (description) {
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
            }
        }
        
        NSString *description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}


- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            else{
                commandDescription = [NSString stringWithFormat:@"Fly to another Star System"]; /////////if current is the first object
            }
                
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            else{
                commandDescription = [NSString stringWithFormat:@"Fly to another Star System"]; ///////// if current is the last object
            }
            
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToAnotherStarSystem: {             /////////////
             commandDescription = [NSString stringWithFormat:@""];
            break;
        }
            
        case   SPIPlayerSpaceshipCommandDestroyCurrentObject: {
            if ([[self.currentSpaceObject destroing]  isEqual: @"YES"])
                commandDescription = [NSString stringWithFormat:@"Destroy %@", [self.currentSpaceObject title]];
            else
                 commandDescription = [NSString stringWithFormat:@"Destroy %@ (locked)", [self.currentSpaceObject title]];
            break;
        }
            
            
        case  SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@"Exit"];
            break;
        }
            
        default:
            break;
    }
    return commandDescription;
}


@end
