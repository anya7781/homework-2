//
//  Planet.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 10/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlanet.h"

@implementation SPIPlanet

- (instancetype)initWithName:(NSString *)name {
    self = [super initWithType:SPISpaceObjectTypePlanet name:name];
    if (self) {
        
    }
    return self;
}

- (void)nextTurn {
    [super nextTurn];
    
    if (self.hasPeoples) {
        self.peoplesCount += ((int)(arc4random() % 1000)) - 200;
    }
}

- (BOOL)hasPeoples {
    return self.peoplesCount > 0;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nPlanet: %@,\nHas atmosphere: %@ \nHas peoples: %@ \nPeoples count: %ld\nDestroing: %@",
                                        self.name,
                                        self.hasAtmosphere ? @"YES" : @"NO",
                                        self.hasPeoples ? @"YES" : @"NO",
                                        self.peoplesCount,
                                        self.dest ? @"YES" : @"NO"];
}

@end
